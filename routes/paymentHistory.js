const express = require('express');
const router = express.Router();
const PaymentHistory = require('../models/PaymentHistory');
const { protect, authorize } = require('../middleware/auth');
const { advancedResults } = require('../middleware/advancedResults');

const {
  getAllPaymentHistory,
  getIndividualMemberPaymentHistory,
} = require('../controllers/paymentHistory');

router.route('/').get(
  protect,
  authorize('owner'),
  advancedResults(PaymentHistory, [
    {
      path: 'memberId',
      select: 'name email phone',
    },
    {
      path: 'packageId',
      select: 'packageName packageCost packageDuration',
    },
  ]),
  getAllPaymentHistory
);
router
  .route('/:id')
  .get(protect, authorize('owner'), getIndividualMemberPaymentHistory);

module.exports = router;
