const express = require('express');
const router = express.Router();
const Package = require('../models/Package');
const { advancedResults } = require('../middleware/advancedResults');
const { protect, authorize } = require('../middleware/auth');

const {
  addPackage,
  updatePackage,
  deletePackage,
  getPackages,
  getPackage,
} = require('../controllers/package');

router
  .route('/')
  .get(protect, authorize('owner'), advancedResults(Package), getPackages)
  .post(protect, authorize('owner'), addPackage);
router
  .route('/:id')
  .get(protect, authorize('owner'), getPackage)
  .put(protect, authorize('owner'), updatePackage)
  .delete(protect, authorize('owner'), deletePackage);
module.exports = router;
