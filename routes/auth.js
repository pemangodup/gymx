const express = require('express');
const router = express.Router();
const {
  register,
  login,
  logout,
  forgotPassword,
  resetPassword,
  checkToken,
  getRefreshToken,
  changePassword,
} = require('../controllers/auth');
const { protect, authorize } = require('../middleware/auth');

router.post('/register', register);
// router.post('/register', protect, authorize('admin'), register);
router.get('/logout', logout);
router.post('/login', login);
router.route('/forgotpassword').post(forgotPassword);
router.route('/checkToken').post(checkToken);
router.route('/resetpassword').put(resetPassword);
router.route('/change-password').post(changePassword);
router.route('/refreshToken').post(getRefreshToken);

module.exports = router;
