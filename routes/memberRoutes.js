const express = require('express');
const router = express.Router();
const { homepage } = require('../controllers/member');
const { protect, authorize } = require('../middleware/auth');

router.route('/').get(homepage);
module.exports = router;
