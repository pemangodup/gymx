const express = require('express');
const router = express.Router();
const { protect, authorize } = require('../middleware/auth');
const {
  sendNotificationToAll,
  sendNotificationToDevices,
  addDevice,
} = require('../controllers/pushNotification');

router.route('/toAll').get(protect, authorize('admin'), sendNotificationToAll);
router
  .route('/toDevices')
  .post(protect, authorize('admin'), sendNotificationToDevices);
router.route('/addDevice').post(protect, authorize('admin'), addDevice);
module.exports = router;
