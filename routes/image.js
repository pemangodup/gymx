const express = require('express');
const router = express.Router({ mergeParams: true });
const { protect, authorize } = require('../middleware/auth');
const {
  getImage,
  uploadImage,
  updateImage,
  deleteImage,
} = require('../controllers/image');

const multer = require('multer');
const upload = multer({ dest: 'public/uploads/' });

router.route('/:id').get(protect, authorize('owner'), getImage);
router
  .route('/:userId')
  .post(protect, authorize('owner'), upload.single('photo'), uploadImage);
router
  .route('/:userId/:imgKey')
  .put(protect, authorize('owner'), upload.single('photo'), updateImage);
router.route('/:userId').delete(protect, authorize('owner'), deleteImage);

module.exports = router;
