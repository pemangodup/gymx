const express = require('express');
const router = express.Router();

const { getHomePage } = require('../controllers/home');
const { protect, authorize } = require('../middleware/auth');

router.route('/').get(protect, authorize('owner'), getHomePage);

module.exports = router;
