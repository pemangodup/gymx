const express = require('express');
const router = express.Router();
const Payment = require('../models/Payment');
const { protect, authorize } = require('../middleware/auth');
const { advancedResults } = require('../middleware/advancedResults');
const {
  makePayment,
  getPayment,
  getPayments,
  getFiveDaysBeforePayment,
  getPaymentDue,
  updatePayment,
  deletePayment,
  getActivePayment,
} = require('../controllers/payment');

router.route('/').get(
  protect,
  authorize('owner'),
  advancedResults(Payment, [
    {
      path: 'packageId',
      select: 'packageName packageCost packageDuration',
    },
    {
      path: 'memberId',
      select: 'name email phone',
    },
  ]),
  getPayments
);
router
  .route('/activePayment')
  .get(protect, authorize('owner'), getActivePayment);
router
  .route('/get-payment-due')
  .get(protect, authorize('owner'), getPaymentDue);

router
  .route('/five-days-left')
  .get(protect, authorize('owner'), getFiveDaysBeforePayment);
router
  .route('/:id')
  .get(protect, authorize('owner'), getPayment)
  .put(protect, authorize('owner'), updatePayment)
  .delete(protect, authorize('owner'), deletePayment);

router.route('/make-payment').post(protect, authorize('owner'), makePayment);

module.exports = router;
