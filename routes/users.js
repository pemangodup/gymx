const express = require('express');
const router = express.Router();
const { protect, authorize } = require('../middleware/auth');
const User = require('../models/User');
const { advancedResults } = require('../middleware/advancedResults');

// Include other resources
const imageRouter = require('./image');

const {
  addUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
} = require('../controllers/users');
const { searchByName } = require('../controllers/users');
router
  .route('/')
  .get(protect, authorize('owner'), advancedResults(User), getUsers)
  .post(protect, authorize('owner'), addUser);
router
  .route('/:id')
  .get(protect, authorize('owner'), getUser)
  .put(protect, authorize('owner'), updateUser)
  .delete(protect, authorize('owner'), deleteUser);
router.route('/searchByName').get(protect, authorize('owner'), searchByName);
module.exports = router;
