const express = require('express');
const router = express.Router();

const {
  createUser,
  uploadImage,
  updateImage,
  getImage,
} = require('../controllers/admin');

const { protect, authorize } = require('../middleware/auth');
const multer = require('multer');
const upload = multer({ dest: 'public/uploads/' });

router.route('/getImage/:userId').get(protect, authorize('owner'), getImage);

router.route('/createUser').post(protect, authorize('admin'), createUser);
router
  .route('/uploadImage/:userId')
  .post(protect, authorize('owner'), upload.single('photo'), uploadImage);
router
  .route('/updateImage/:userId/:imgKey')
  .put(protect, authorize('owner'), upload.single('photo'), updateImage);

module.exports = router;
