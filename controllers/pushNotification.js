const PaymentHistory = require('../models/PaymentHistory');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const { sendNotification, addDevice } = require('../utils/pushNotification');

// @desc     Send Push Notification To All
// @route    POST /dealpha/v1/notification/toAll
// @access   Private
exports.addDevice = asyncHandler(async (req, res, next) => {
  const userId = req.body.userId;
  var user = {
    app_id: process.env.APP_ID,
    external_user_id: '61f2afedf8ace815014a46dd',
    tags: { tag_key: 'First', key2: 'attempt' },
    device_type: 11,
    identifier: 'pngodup123@gmail.com',
  };
  const response = await addDevice(user);
  res.status(200).json({
    success: true,
    message: '',
    data: 'hehe',
  });
});

// @desc     Send Push Notification To All
// @route    POST /dealpha/v1/notification/toAll
// @access   Private

exports.sendNotificationToAll = asyncHandler(async (req, res, next) => {
  var message = {
    app_id: process.env.APP_ID,
    contents: { en: 'English Message' },
    // included_segments: ['Subscribed Users'],
    included_segments: ['All'],
    content_available: true,
    small_icon: 'ic_notification_icon',
    data: {
      PushTitle: 'CUSTOM NOTIFICATION',
    },
  };

  const data = await sendNotification(message);
  if (!data) {
    return next(new ErrorResponse(`Notification error`, 400));
  }
  res.status(200).json({
    success: true,
    message: '',
    data: data,
  });
});

// @desc     Send Push Notification To Devices
// @route    POST /dealpha/v1/notification/toDevices
// @access   Private

exports.sendNotificationToDevices = asyncHandler(async (req, res, next) => {
  var message = {
    app_id: process.env.APP_ID,
    contents: { en: 'English Message' },
    // included_segments: ['Subscribed Users'],
    //included_segments: ['included_player_ids'],
    include_player_ids: req.body.devices,
    email_subject: 'Welcome to Cat Facts!',
    email_body:
      "<html><head>Welcome to Cat Facts</head><body><h1>Welcome to Cat Facts<h1><h4>Learn more about everyone's favorite furry companions!</h4><hr/><p>Hi Nick,</p><p>Thanks for subscribing to Cat Facts! We can't wait to surprise you with funny details about your favorite animal.</p><h5>Today's Cat Fact (March 27)</h5><p>In tigers and tabbies, the middle of the tongue is covered in backward-pointing spines, used for breaking off and gripping meat.</p><a href='https://catfac.ts/welcome'>Show me more Cat Facts</a><hr/><p><small>(c) 2018 Cat Facts, inc</small></p><p><small><a href='[unsubscribe_url]'>Unsubscribe</a></small></p></body></html>",
    content_available: true,
    small_icon: 'ic_notification_icon',
    data: {
      PushTitle: 'CUSTOM NOTIFICATION',
    },
  };

  const data = await sendNotification(message);
  if (!data) {
    return next(new ErrorResponse(`Notification error`, 400));
  }
  res.status(200).json({
    success: true,
    message: '',
    data: data,
  });
});
