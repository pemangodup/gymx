const Package = require('../models/Package');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc     Get Package
// @route    GET /dealpha/v1/package
// @access   Public
exports.getPackages = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc     Get Single Package
// @route    GET /dealpha/v1/package
// @access   Public
exports.getPackage = asyncHandler(async (req, res, next) => {
  const id = req.params.id;

  const package = await Package.findById(id);
  if (!package) {
    return next(new ErrorResponse('No package found with the id.', 404));
  }

  res.status(200).json({
    success: true,
    message: '',
    data: package,
  });
});

// @desc     Insert Package
// @route    POST /dealpha/v1/package
// @access   Private
exports.addPackage = asyncHandler(async (req, res, next) => {
  const { packageName, packageCost, packageDuration } = req.body;
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  // Checking whether package already exist or not
  let package = await Package.findOne({
    gymId: { $eq: loggedInUserId },
    packageName,
  });

  if (!package) {
    // Insert package details into database
    package = await Package.create({
      gymId: loggedInUserId,
      packageName,
      packageCost,
      packageDuration,
    });
  } else {
    return next(new ErrorResponse('Package name already taken', 409));
  }

  package = res.status(200).json({
    success: true,
    message: '',
    data: package,
  });
});

// @desc     Update Package
// @route    PUT /dealpha/v1/package/:id
// @access   Private
exports.updatePackage = asyncHandler(async (req, res, next) => {
  const id = req.params.id;
  const data = { ...req.body };

  // Checking whether package exist or not
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let package = await Package.find({ _id: id, gymId: { $eq: loggedInUserId } });
  // If package does not exist
  if (package.length == 0) {
    return next(new ErrorResponse('Package does not exist', 404));
  }

  package = await Package.findByIdAndUpdate(id, data, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    message: '',
    data: package,
  });
});

// @desc     Delete Package
// @route    DELETE /dealpha/v1/package/:id
// @access   Private
exports.deletePackage = asyncHandler(async (req, res, next) => {
  const id = req.params.id;

  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let package = await Package.find({ _id: id, gymId: { $eq: loggedInUserId } });

  if (package.length == 0) {
    return next(new ErrorResponse('No package found', 404));
  }
  package = await Package.findById(id);

  package.remove();
  res.status(200).json({
    success: true,
    message: '',
    data: {},
  });
});
