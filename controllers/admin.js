const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const Admin = require('../models/Admin');
const { uploadFile, deleteFile } = require('../utils/s3');
const fs = require('fs');
const util = require('util');
const unlinkFile = util.promisify(fs.unlink);

// @desc     Insert Image
// @route    POST /dealpha/v1/admin/getImage/:userId
// @access   Private
exports.getImage = asyncHandler(async (req, res, next) => {
  const userId = req.params.userId;
  if (!userId) {
    return next(new ErrorResponse('User id is empty', 400));
  }

  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  const userImage = await Admin.find({
    _id: userId,
    gymId: { $eq: loggedInUserId },
  }).select('photo');

  if (userImage.length === 0) {
    return next(new ErrorResponse('No user found.', 404));
  }
  res.status(200).json({
    success: true,
    message: '',
    data: userImage,
  });
});

// @desc     Create Main User
// @route    POST /dealpha/v1/admin/createUser
// @access   Private
exports.createUser = asyncHandler(async (req, res, next) => {
  const data = req.body;
  const email = data.email;
  const user = await Admin.findOne({ email });

  if (user) {
    return next(new ErrorResponse('User already exists', 400));
  } else {
    const user = await Admin.create(data);
    res.status(200).json({
      success: true,
      message: '',
      data: user,
    });
  }
});

// @desc     Upload DP Main User
// @route    POST /dealpha/v1/admin/uploadImage
// @access   Private
exports.uploadImage = asyncHandler(async (req, res, next) => {
  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let user = await Admin.find({
    _id: req.params.userId,
    gymId: loggedInUserId,
  });
  if (user.length === 0) {
    return next(new ErrorResponse('User not found', 404));
  }

  if (!req.file) {
    return next(new ErrorResponse('Please upload a file', 400));
  }
  const file = req.file;

  // Check file size
  if (file.size > process.env.MAX_FILE_UPLOAD) {
    return next(
      new ErrorResponse(
        `Please upload image less than ${process.env.MAX_FILE_UPLOAD}`
      )
    );
  }
  // Compressing the image
  const result = await uploadFile(file);

  // Deleting the image once it is uploaded in the aws s3
  await unlinkFile(file.path);
  console.log(result);
  await Admin.findByIdAndUpdate(req.params.userId, {
    photo: { location: result.Location, key: result.Key },
  });

  res.status(200).json({
    success: true,
    message: '',
    data: file,
  });
});

// @desc     Update Image
// @route    PUT /dealpha/v1/admin/updateImage/:userId/:key
// @access   Private
exports.updateImage = asyncHandler(async (req, res, next) => {
  const userId = req.params.userId;
  const key = req.params.imgKey;
  if (!userId && !key) {
    return next(new ErrorResponse('Insert userId and keyId', 404));
  }
  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let user = await Admin.find({
    _id: req.params.userId,
    gymId: loggedInUserId,
  });
  if (user.length === 0) {
    return next(new ErrorResponse('User not found', 404));
  }

  if (key != user[0].photo.key) {
    return next(new ErrorResponse('s3 key does not match', 404));
  }
  await deleteFile(key);
  const file = req.file;
  console.log(file);
  const result = await uploadFile(file);
  await unlinkFile(file.path);
  console.log(result);

  await Admin.findByIdAndUpdate(req.params.userId, {
    photo: { location: result.Location, key: result.Key },
  });
  res.status(200).json({
    success: true,
    message: '',
    data: file,
  });
});

// @desc     Delete Image
// @route    DELETE /dealpha/v1/admin/deleteImage/:userId
// @access   Private
exports.deleteImage = asyncHandler(async (req, res, next) => {
  const userId = req.params.userId;
  const key = req.body.imgKey;

  if (!userId && !key) {
    return next(new ErrorResponse('Insert userId and keyId', 404));
  }

  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let user = await Admin.find({
    _id: req.params.userId,
    gymId: loggedInUserId,
  });
  if (user.length === 0) {
    return next(new ErrorResponse('User not found', 404));
  }

  if (key != user[0].photo.key) {
    return next(new ErrorResponse('s3 key does not match', 404));
  }
  await deleteFile(key);
  await Admin.findByIdAndUpdate(req.params.userId, {
    photo: { location: '', key: '' },
  });
  res.status(200).json({
    success: true,
    message: '',
  });
});
