const Admin = require('../models/Admin');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const sendMail = require('../utils/sendEmail');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs/dist/bcrypt');
const User = require('../models/User');

// @desc    Register user
// @route   POST /dealpha/v1/auth/register
// @access  Public
exports.register = asyncHandler(async (req, res, next) => {
  const data = req.body;
  const email = data.email;
  const user = await Admin.findOne({ email });
  if (user) {
    return next(new ErrorResponse('User already exists', 400));
  } else {
    const user = await Admin.create({ data });

    res.status(200).json({
      success: true,
      data: user,
    });
  }
});

// @desc    Login
// @route   POST /dealpha/v1/auth/login
// @access  Private
exports.login = asyncHandler(async (req, res, next) => {
  const { role, email, password } = req.body;

  // Validate email and password
  if (!email || !password) {
    return next(new ErrorResponse('Please provide both the credentials', 400));
  }
  // Check the user
  let user;
  if (role == 'member') {
    console.log('member');
    user = await User.findOne({ email }).select('+password');
  } else {
    console.log('admin');
    user = await Admin.findOne({ email }).select('+password');
  }

  // User existence
  if (!user) {
    return next(new ErrorResponse('Username credential is wrong', 401));
  }

  if (user.loginStatus == false) {
    return next(
      res.status(200).json({
        success: true,
        message: 'Change password',
        email: email,
        accessToken: '',
        refreshToken: '',
      })
    );
  }

  // Check password
  const isMatch = await user.comparePassword(password);

  // Password does not match
  if (!isMatch) {
    return next(new ErrorResponse('Password does not match', 403));
  }

  // Create token and send response
  sendTokenResponse(user, 200, res);
});

// @desc     Forgot password
// @route    POST /dealpha/v1/auth/forgotpassword
// @access   Private
exports.forgotPassword = asyncHandler(async (req, res, next) => {
  const user = await Admin.findOne({ email: req.body.email });

  if (!user) {
    return next(new ErrorResponse('Wrong email id', 404));
  }

  const resetToken = user.getResetPasswordToken();

  await user.save({ validateBeforeSave: false });

  // Create reset url
  // const resetUrl = `${req.protocol}://${req.get(
  //   'host'
  // )}/dealpha/v1/auth/resetPassword/${resetToken}`;

  // const message = `You are receiving this email because
  // you (or some else) has requested the reset of a password.
  // Please make a PUT request to: \n\n${resetUrl}`;
  const message = `You are receiving this email because 
  you (or some else) has requested the reset of a password. 
  Please make a PUT request to: \n\n${resetToken}`;

  try {
    await sendMail({
      email: user.email,
      subject: 'Password reset token',
      message,
    });
    res.status(200).json({
      success: true,
      message: '',
      otp: resetToken,
    });
  } catch (error) {
    console.log(error);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpiration = undefined;
    await user.save({ validateBeforeSave: false });
    await next(new ErrorResponse('Email could not be sent', 500));
  }
});

// @desc     Check Token
// @route    POST /dealpha/v1/auth/checkToken
// @access   Private
exports.checkToken = asyncHandler(async (req, res, next) => {
  const resetPasswordToken = req.body.token;
  if (!resetPasswordToken) {
    return next(new ErrorResponse('Enter token. Token field is empty', 204));
  }

  const user = await Admin.findOne({
    resetPasswordToken,
    resetPasswordExpiration: { $gt: Date.now() },
  });

  if (!user) {
    return next(new ErrorResponse('Invalid token', 400));
  }
  res.status(200).json({
    success: true,
    message: '',
    token: resetPasswordToken,
  });
});

// @desc     Reset Password
// @route    POST /dealpha/v1/auth/resetpassword
// @access   Private
exports.resetPassword = asyncHandler(async (req, res, next) => {
  // Get hashed token
  // const resetPasswordToken = crypto
  //   .createHash('sha256')
  //   .update(req.params.resetToken)
  //   .digest('hex');
  const resetPasswordToken = req.body.token;

  const user = await Admin.findOne({
    resetPasswordToken,
    resetPasswordExpiration: { $gt: Date.now() },
  });

  if (!user) {
    return next(new ErrorResponse('Invalid token', 400));
  }
  // Set the new password
  user.password = req.body.password;
  user.resetPasswordToken = undefined;
  user.resetPasswordExpiration = undefined;

  await user.save();
  sendTokenResponse(user, 200, res);
});

// @desc     Change password
// @route    POST /dealpha/v1/auth/change-password
// @access   Private
exports.changePassword = asyncHandler(async (req, res, next) => {
  const { email, oldPassword, newPassword } = req.body;
  let user = await Admin.findOne({ email }).select('+password');
  if (!user) {
    return next(new ErrorResponse('No user found', 404));
  }
  // Check password
  const isMatch = await user.comparePassword(oldPassword);

  if (!isMatch) {
    return next(new ErrorResponse('Password does not match', 401));
  }
  const salt = await bcrypt.genSalt(12);
  const password = await bcrypt.hash(newPassword, salt);
  user = await Admin.findByIdAndUpdate(
    user.id,
    { password, loginStatus: true },
    { new: true, runValidators: true }
  );
  if (!user) {
    return next(new ErrorResponse('No user found', 404));
  }

  // Create token and send response
  sendTokenResponse(user, 200, res);
});

// @desc     Refresh Token
// @route    POST /dealpha/v1/auth/refreshtoken
// @access   Private
exports.getRefreshToken = asyncHandler(async (req, res, next) => {
  const refreshToken = req.body.refreshToken;

  if (!refreshToken || !refreshToken.includes(refreshToken)) {
    return next(new ErrorResponse('No refresh token', 403));
  }

  jwt.verify(
    refreshToken,
    process.env.REFRESH_TOKEN_SECRET,
    async (err, user) => {
      if (!user) {
        return next(
          new ErrorResponse('No user found as refresh token is expired', 500)
        );
      }
      user = await Admin.findOne({ _id: user.id });

      if (!err) {
        const accessToken = user.getJwtAccessToken();
        const newRefreshToken = user.getJwtRefreshToken();
        // Response
        return res.status(200).cookie('accessToken', accessToken).json({
          success: true,
          accessToken,
          refreshToken: newRefreshToken,
        });
      } else {
        return res.status(200).json({
          success: false,
          message: err.message,
        });
      }
    }
  );
});

// Get token from model, create cookie and send response
const sendTokenResponse = function (user, statusCode, res) {
  // Create token
  const accessToken = user.getJwtAccessToken();
  const refreshToken = user.getJwtRefreshToken();

  const options = {
    expires: new Date(
      Date.now() + process.env.JWT_EXPIRES * 24 * 60 * 60 * 1000
    ),
    httpOnly: true,
  };

  // For production mode
  if (process.env.NODE_ENV === 'production') {
    options.secure = true;
  }

  // Response
  res.status(200).cookie('accessToken', accessToken, options).json({
    success: true,
    message: '',
    email: user.email,
    name: user.userName,
    image: user.photo.location,
    accessToken,
    refreshToken,
  });
};

// @desc     Logout user / clear cookies
// @route    GET /api/v1/auth/logout
// @access   Private
exports.logout = asyncHandler(async (req, res, next) => {
  res.cookie('token', 'none', {
    expires: new Date(Date.now() + 10 * 10000),
    httpOnly: true,
  });
  res.status(200).json({
    success: true,
    data: {},
  });
});
