const User = require('../models/User');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const { deleteFile } = require('../utils/s3');

// @desc    Add user
// @route   POST /dealpha/v1/users
// @access  Private
exports.addUser = asyncHandler(async (req, res, next) => {
  let data = req.body;
  data.gymId = req.user._id;
  const email = data.email;
  const user = await User.findOne({ email });
  if (user) {
    return next(new ErrorResponse('User already exists', 400));
  } else {
    const user = await User.create(data);

    res.status(200).json({
      success: true,
      data: user,
    });
  }
});
// @desc     Get Users
// @route    GET /dealpha/v1/users
// @access   Public
exports.getUsers = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc     Get Users
// @route    GET /dealpha/v1/users
// @access   Public
exports.getUser = asyncHandler(async (req, res, next) => {
  const id = req.params.id;
  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  const user = await User.find({
    _id: id,
    gymId: { $eq: loggedInUserId },
  });
  res.status(200).json({
    success: true,
    message: '',
    data: user,
  });
});

// @desc     Update User
// @route    PUT /dealpha/v1/users/:id
// @access   Private
exports.updateUser = asyncHandler(async (req, res, next) => {
  const userId = req.params.id;

  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let user = await User.find({
    _id: userId,
    gymId: { $eq: loggedInUserId },
  });
  if (user.length === 0) {
    return next(new ErrorResponse('User not found', 404));
  }

  user = await User.findByIdAndUpdate(userId, req.body, {
    new: true,
    runValidators: true,
  });
  res.status(200).json({
    success: true,
    message: '',
    data: user,
  });
});

// @desc     Delete User
// @route    DELETE /dealpha/v1/users/:id
// @access   Private
exports.deleteUser = asyncHandler(async (req, res, next) => {
  const userId = req.params.id;

  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let user = await User.find({
    _id: userId,
    gymId: loggedInUserId,
  });
  if (user.length === 0) {
    return next(new ErrorResponse('User not found', 404));
  }

  user = await User.findById(userId);
  if (user.photo.key) {
    const key = user.photo.key;
    await deleteFile(key);
  }

  // Using remove in order for cascade to work
  user.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});

// @desc     Search by name
// @route    GET /dealpha/v1/users/searchByName/:name
// @access   Private
exports.searchByName = asyncHandler(async (req, res, next) => {
  const searchField = req.query.name;
  const loggedInUserId = JSON.stringify(req.user._id).slice(1, -1);
  const users = await User.find({
    gymId: { $eq: loggedInUserId },
    name: { $regex: searchField, $options: 'i' },
  });
  if (!searchField) {
    res.status(200).json({
      success: true,
      message: '',
      data: [],
    });
    // return next(new ErrorResponse('No data found to display', 404));
  } else {
    res.status(200).json({
      success: true,
      message: '',
      data: users,
    });
  }
});
