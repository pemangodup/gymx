const Payment = require('../models/Payment');
const PaymentHistory = require('../models/PaymentHistory');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const User = require('../models/User');
const {
  advancedResultsMultiplePopulate,
} = require('../middleware/advancedResults');

// @desc     Get Single Payment
// @route    GET /dealpha/v1/payment/:id
// @access   Private
exports.getPayment = asyncHandler(async (req, res, next) => {
  const memberId = req.params.id;

  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let payment = await Payment.find({
    memberId,
    gymId: loggedInUserId,
  });
  if (payment.length === 0) {
    return next(new ErrorResponse('Payment not found', 404));
  }

  payment = await Payment.findOne({ memberId })
    .populate({
      path: 'packageId',
      select: 'packageName packageCost packageDuration',
    })
    .populate({
      path: 'memberId',
      select: 'name email phone',
    });
  if (!payment) {
    return next(new ErrorResponse('Payment detail not found.', 404));
  }
  res.status(200).json({
    success: true,
    message: '',
    data: payment,
  });
});

// @desc     Get All Payments
// @route    GET /dealpha/v1/payment
// @access   Private
exports.getPayments = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc     Get Active Payments
// @route    GET /dealpha/v1/payment/activePayment
// @access   Private
exports.getActivePayment = asyncHandler(async (req, res, next) => {
  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  const presentDate = new Date();
  const payment = await Payment.find({
    gymId: loggedInUserId,
    expiryDate: { $gt: presentDate },
  });
  if (payment.length === 0) {
    return next(new ErrorResponse('No active payment available'));
  }
  res.status(200).json({
    success: true,
    message: '',
    data: payment,
  });
});

// @desc     Update Payment
// @route    PUT /dealpha/v1/payment/:id
// @access   Private
exports.updatePayment = asyncHandler(async (req, res, next) => {
  const paymentId = req.params.id;
  const body = req.body;

  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let payment = await Payment.find({
    _id: paymentId,
    gymId: { $eq: loggedInUserId },
  });
  if (payment.length === 0) {
    return next(new ErrorResponse('Payment not found', 404));
  }
  // Getting updated in the payment model
  payment = await Payment.findByIdAndUpdate(paymentId, body, {
    new: true,
    runValidators: true,
  });

  // if it is an update should get updated in the history model as well
  await PaymentHistory.findByIdAndUpdate(paymentId, body, {
    new: true,
    runValidators: true,
  });
  res.status(200).json({
    success: true,
    message: '',
    data: payment,
  });
});

// @desc     Make Payment
// @route    POST /dealpha/v1/payment/:id
// @access   Private
exports.makePayment = asyncHandler(async (req, res, next) => {
  let payment;
  const body = req.body;
  const memberId = body.memberId;

  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  // Inserting gym id in payment
  body.gymId = loggedInUserId;

  const user = await User.find({
    _id: memberId,
    gymId: { $eq: loggedInUserId },
  });
  if (user.length == 0) {
    return next(
      new ErrorResponse('No user found with given memeber id or gymid', 404)
    );
  }

  const paymentHistory = await Payment.findOne({
    memberId,
    gymId: { $eq: loggedInUserId },
  });

  if (paymentHistory) {
    payment = await Payment.findByIdAndDelete(paymentHistory.id);
    payment = await Payment.create(body);
    body._id = payment.id;
    await PaymentHistory.create(body);
  } else {
    payment = await Payment.create(body);
    body._id = payment.id;
    await PaymentHistory.create(body);
  }
  res.status(200).json({
    success: true,
    message: '',
    data: payment,
  });
});

// @desc     Delete Payment
// @route    GET /dealpha/v1/payment/deletePayment/:id
// @access   Private
exports.deletePayment = asyncHandler(async (req, res, next) => {
  const paymentId = req.params.id;

  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let payment = await Payment.find({
    _id: paymentId,
    gymId: loggedInUserId,
  });
  if (payment.length === 0) {
    return next(new ErrorResponse('Payment not found', 404));
  }

  payment = await Payment.findById(paymentId);

  if (!payment) {
    return next(new ErrorResponse('No payment found with the given id.', 404));
  }
  await payment.remove();
  await PaymentHistory.findByIdAndDelete(paymentId);
  res.status(200).json({
    success: true,
    message: '',
    data: {},
  });
});

// @desc     Get payment details of five days left
// @route    GET /dealpha/v1/payment/five-days-left
// @access   Private
exports.getFiveDaysBeforePayment = asyncHandler(async (req, res, next) => {
  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  var presentDate = new Date();
  presentDate.setDate(presentDate.getDate());
  const payments = await Payment.find({
    gymId: loggedInUserId,
    expiryDate: { $gt: presentDate },
  })
    .populate({ path: 'memberId', select: ' name email phone' })
    .populate({
      path: 'packageId',
      select: 'packageName packageCost packageDuration',
    });
  let totalPayment = [];
  if (payments.length === 0) {
    return next(new ErrorResponse('No five days due payment.', 404));
  }
  payments.forEach(async (element) => {
    var diff = parseInt(
      (element.expiryDate - presentDate) / (1000 * 60 * 60 * 24),
      10
    );
    if (diff <= 5 && diff >= 0) {
      totalPayment.push(element);
    }
  });

  if (!payments[0]) {
    return next(new ErrorResponse('No pending payment.'));
  }
  // For sorting in paid date. Lates data on the top
  totalPayment.sort((a, b) => new Date(b.paidDate) - new Date(a.paidDate));
  res.status(200).json({
    success: true,
    message: '',
    count: totalPayment.length,
    data: totalPayment,
  });
});

// @desc     Get Expired Payment
// @route    GET /dealpha/v1/payment/get-payment-due
// @access   Private
exports.getPaymentDue = asyncHandler(async (req, res, next) => {
  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  var presentDate = new Date();
  presentDate.setDate(presentDate.getDate());
  const payments = await Payment.find({
    gymId: loggedInUserId,
    expiryDate: { $lt: presentDate },
  })
    .populate({ path: 'memberId', select: ' name email phone' })
    .populate({
      path: 'packageId',
      select: 'packageName packageCost packageDuration',
    });

  // For sorting in paid date. Lates data on the top
  payments.sort((a, b) => new Date(b.paidDate) - new Date(a.paidDate));

  res.status(200).json({
    success: true,
    message: '',
    count: payments.length,
    data: payments,
  });
});
