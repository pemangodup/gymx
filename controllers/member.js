const User = require('../models/User');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc     View Home
// @route    Get /dealpha/v1/member
// @access   Private
exports.homepage = asyncHandler(async (req, res, next) => {
  console.log('Homepage');
  console.log('hehe');
  res.status(200).json({
    success: true,
    message: '',
    data: '',
  });
});
