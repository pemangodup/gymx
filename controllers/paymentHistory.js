const PaymentHistory = require('../models/PaymentHistory');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');

// @desc     Get All Payment History
// @route    GET /dealpha/v1/paymenthistory
// @access   Private
exports.getAllPaymentHistory = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

// @desc     Get  payment history of single user
// @route    GET /dealpha/v1/payment/:id
// @access   Private
exports.getIndividualMemberPaymentHistory = asyncHandler(
  async (req, res, next) => {
    const memberId = req.params.id;

    // Logged in user
    let loggedInUserId = JSON.stringify(req.user._id);
    loggedInUserId = loggedInUserId.slice(1, -1);

    const paymentHistory = await PaymentHistory.find({
      gymId: loggedInUserId,
      memberId,
    })
      .populate({
        path: 'memberId',
        select: 'name email phone',
      })
      .populate({
        path: 'packageId',
        select: 'packageName packageCost packageDuration',
      });
    if (paymentHistory.length == 0) {
      return next(
        new ErrorResponse(
          'No payment history of the user or user does not exist.',
          404
        )
      );
    }

    // For sorting in paid date. Lates data on the top
    paymentHistory.sort((a, b) => new Date(b.paidDate) - new Date(a.paidDate));

    res.status(200).json({
      success: true,
      message: '',
      count: paymentHistory.length,
      data: paymentHistory,
    });
  }
);
