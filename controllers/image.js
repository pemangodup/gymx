const User = require('../models/User');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const path = require('path');
const { uploadFile, deleteFile } = require('../utils/s3');
const fs = require('fs');
const util = require('util');
const unlinkFile = util.promisify(fs.unlink);
const sharp = require('sharp');

// @desc     Insert Image
// @route    POST /dealpha/v1/image/:userId
// @access   Private
exports.getImage = asyncHandler(async (req, res, next) => {
  const userId = req.params.id;
  if (!userId) {
    return next(new ErrorResponse('User id is empty', 400));
  }

  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  const userImage = await User.find({
    _id: userId,
    gymId: { $eq: loggedInUserId },
  }).select('photo');

  if (userImage.length === 0) {
    return next(new ErrorResponse('No user found.', 404));
  }
  res.status(200).json({
    success: true,
    message: '',
    data: userImage,
  });
});

// @desc     Insert Image
// @route    POST /dealpha/v1/image/:userId
// @access   Private
exports.uploadImage = asyncHandler(async (req, res, next) => {
  // Logged in user

  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let user = await User.find({
    _id: req.params.userId,
    gymId: loggedInUserId,
  });
  if (user.length === 0) {
    return next(new ErrorResponse('User not found', 404));
  }

  if (!req.file) {
    return next(new ErrorResponse('Please upload a file', 400));
  }
  const file = req.file;
  console.log(file);
  // Make sure file is an image
  // if (!file.mimetype.startsWith('image')) {
  //   return next(new ErrorResponse('Please insert file type of image', 400));
  // }

  // Check file size
  if (file.size > process.env.MAX_FILE_UPLOAD) {
    return next(
      new ErrorResponse(
        `Please upload image less than ${process.env.MAX_FILE_UPLOAD}`
      )
    );
  }
  // Compressing the image
  const result = await uploadFile(file);

  // Deleting the image once it is uploaded in the aws s3
  await unlinkFile(file.path);
  console.log(result);
  await User.findByIdAndUpdate(req.params.userId, {
    photo: { location: result.Location, key: result.Key },
  });
  res.status(200).json({
    success: true,
    message: '',
    data: file,
  });
});

// @desc     Update Image
// @route    PUT /dealpha/v1/image/:userId/:key
// @access   Private
exports.updateImage = asyncHandler(async (req, res, next) => {
  const userId = req.params.userId;
  const key = req.params.imgKey;
  if (!userId && !key) {
    return next(new ErrorResponse('Insert userId and keyId', 404));
  }
  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let user = await User.find({
    _id: req.params.userId,
    gymId: loggedInUserId,
  });
  if (user.length === 0) {
    return next(new ErrorResponse('User not found', 404));
  }

  if (key != user[0].photo.key) {
    return next(new ErrorResponse('s3 key does not match', 404));
  }
  await deleteFile(key);
  const file = req.file;
  console.log(file);
  const result = await uploadFile(file);
  await unlinkFile(file.path);
  console.log(result);

  await User.findByIdAndUpdate(req.params.userId, {
    photo: { location: result.Location, key: result.Key },
  });
  res.status(200).json({
    success: true,
    message: '',
    data: file,
  });
});

// @desc     Delete Image
// @route    DELETE /dealpha/v1/image/:userId
// @access   Private
exports.deleteImage = asyncHandler(async (req, res, next) => {
  const userId = req.params.userId;
  const key = req.body.imgKey;

  if (!userId && !key) {
    return next(new ErrorResponse('Insert userId and keyId', 404));
  }

  // Logged in user
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  let user = await User.find({
    _id: req.params.userId,
    gymId: loggedInUserId,
  });
  if (user.length === 0) {
    return next(new ErrorResponse('User not found', 404));
  }

  if (key != user[0].photo.key) {
    return next(new ErrorResponse('s3 key does not match', 404));
  }
  await deleteFile(key);
  await User.findByIdAndUpdate(req.params.userId, {
    photo: { location: '', key: '' },
  });
  res.status(200).json({
    success: true,
    message: '',
  });
});
