const Payment = require('../models/Payment');
const Package = require('../models/Package');
const PaymentHistory = require('../models/PaymentHistory');
const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const User = require('../models/User');

exports.getHomePage = asyncHandler(async (req, res, next) => {
  let loggedInUserId = JSON.stringify(req.user._id);
  loggedInUserId = loggedInUserId.slice(1, -1);

  // To get sum of value from single month
  const data = await PaymentHistory.find({
    gymId: { $eq: loggedInUserId },
    paidDate: {
      $gte: new Date(new Date().getFullYear(), 0, 1),
      $lte: new Date(new Date().getFullYear(), 11, 31),
    },
  });
  const sumPerMonth = getMonthlySum(data);

  const newData = { year: new Date().getFullYear(), monthlyData: sumPerMonth };

  const totalLifetimeMember = (
    await User.find({ gymId: loggedInUserId, role: { $ne: 'owner' } })
  ).length;

  const numberOfPackage = (await Package.find({ gymId: loggedInUserId }))
    .length;
  const activeMember = (
    await Payment.find({
      gymId: loggedInUserId,
      expiryDate: { $gt: new Date() },
    })
  ).length;
  const inactiveMember = totalLifetimeMember - activeMember;

  const home = {
    totalLifetimeMember,
    activeMember,
    inactiveMember,
    numberOfPackage,
    yearlyData: [newData],
  };
  await res.status(200).json({
    success: true,
    message: '',
    data: home,
  });
});

function getMonthlySum(data) {
  const sum = data.reduce((acc, cur) => {
    acc[cur.paidDate.toISOString().slice(5, 7)] =
      acc[cur.paidDate.toISOString().slice(5, 7)] + cur.amount || cur.amount;
    return acc;
  }, {});

  return sum;
}
