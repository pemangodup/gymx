const mongoose = require('mongoose');

const PaymentHistorySchema = new mongoose.Schema({
  gymId: {
    type: String,
    required: [true, 'No user logged in'],
  },
  paidDate: { type: Date, default: Date.now },
  renewDate: {
    type: Date,
    required: [true, 'Insert renewed date.'],
  },
  expiryDate: { type: Date, required: [true, 'Insert eexpiry date.'] },
  amount: { type: Number, required: [true, 'Please insert a number'] },
  packageId: {
    type: mongoose.Schema.ObjectId,
    ref: 'Package',
    required: [true, 'No package selected'],
  },
  memberId: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: [true, 'No member selected'],
  },
});

module.exports = mongoose.model('PaymentHistory', PaymentHistorySchema);
