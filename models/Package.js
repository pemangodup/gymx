const mongoose = require('mongoose');

const PackageSchema = new mongoose.Schema({
  gymId: {
    type: String,
    required: [true, 'No user logged in'],
  },
  packageName: {
    type: String,
    required: [true, 'Insert package name.'],
  },
  packageCost: {
    type: Number,
    required: [true, 'Insert the cost'],
  },
  packageDuration: {
    type: Number,
    required: [true, 'Insert a package'],
    min: [1, 'It is less than 1'],
    max: [12, 'It is more than 12'],
  },
});

module.exports = mongoose.model('Package', PackageSchema);
