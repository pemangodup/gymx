const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const AdminSchema = new mongoose.Schema({
  userName: {
    type: String,
    required: [true, 'No user added'],
    trim: true,
    maxlength: [30, 'Max name length is 30'],
  },
  email: {
    type: String,
    match: [
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      'Please enter correct email',
    ],
    unique: [true, 'Email is already taken'],
  },
  password: {
    type: String,
    require: [true, 'Password field is empty'],
    minlength: [6, 'It is less than 6 character'],
    select: false,
  },
  photo: {
    location: {
      type: String,
      default: '',
    },
    key: {
      type: String,
      default: '',
    },
  },
  address: {
    type: String,
    required: [true, 'Address field is empty'],
    maxlength: [150, 'Maximum character is 150'],
  },
  phone: {
    type: Number,
    unique: [true, 'Phone number already exists'],
    maxlength: [10, 'Please add a phone number'],
  },
  photo: {
    location: {
      type: String,
      default: '',
    },
    key: {
      type: String,
      default: '',
    },
  },
  role: {
    type: String,
    enum: ['owner', 'trainer', 'admin'],
    default: 'owner',
  },
  loginStatus: {
    type: Boolean,
    default: false,
  },
  resetPasswordToken: String,
  resetPasswordExpiration: Date,
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

// Encrypt password using bcrypt
AdminSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next();
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

// Compare password
AdminSchema.methods.comparePassword = async function (enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};

// Getting json access token and returning it
AdminSchema.methods.getJwtAccessToken = function () {
  return jwt.sign(
    {
      id: this._id,
    },
    process.env.JWT_SECRET,
    { expiresIn: process.env.JWT_EXPIRES }
  );
};

// Getting json refresh token and returning it
AdminSchema.methods.getJwtRefreshToken = function () {
  return jwt.sign({ id: this._id }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: process.env.REFRESH_TOKEN_SECRET_EXPIRES,
  });
};

// Cascade delete when user is deleted
AdminSchema.pre('remove', async function (next) {
  await this.model('Payment').deleteMany({ memberId: this._id });
  next();
});

// Getting the reset password token from crypto, resetting it and returning it
AdminSchema.methods.getResetPasswordToken = function () {
  this.resetPasswordToken = Math.floor(1000 + Math.random() * 9000);

  // Set expire
  this.resetPasswordExpiration = Date.now() + 10 * 60 * 1000;
  return this.resetPasswordToken;
};

module.exports = mongoose.model('Admin', AdminSchema);
