const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

const UserSchema = new mongoose.Schema(
  {
    gymId: {
      type: String,
      required: [true, 'Gym id is not inserted'],
      trim: true,
    },
    name: {
      type: String,
      required: [true, 'Please enter a name'],
      trim: true,
      maxlength: [30, 'Max length is 30'],
    },
    email: {
      type: String,
      match: [
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        'Please enter correct email',
      ],
      unique: [true, 'Email is already taken'],
    },
    // password: {
    //   type: String,
    //   // require: [true, 'Please add a password'],
    //   // minlength: 6,
    //   select: false,
    //   default: '123456',
    // },
    password: {
      type: String,
      default: '123456',
    },
    phone: {
      type: Number,
      unique: [true, 'Phone number already exists'],
      maxlength: [10, 'Please add a phone number'],
    },
    gender: {
      type: String,
      enum: ['male', 'female', 'others'],
      required: true,
    },
    dob: { type: Date, required: [true, 'Date of birth is required'] },
    address: {
      type: String,
      maxlength: [100, 'Max length of the address is 100'],
      required: [true, 'Please add an address'],
    },
    work: {
      type: String,
      enum: ['work', 'student', 'both'],
    },
    photo: {
      location: {
        type: String,
        default: '',
      },
      key: {
        type: String,
        default: '',
      },
    },
    role: {
      type: String,
      enum: ['admin', 'trainer', 'member'],
      default: 'member',
    },
    loginStatus: {
      type: Boolean,
      default: false,
    },
    oneSignalId: String,
    resetPasswordToken: String,
    resetPasswordExpiration: Date,
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

// Reverse populate with virtuals
UserSchema.virtual('payment', {
  ref: 'Payment',
  localField: '_id',
  foreignField: 'memberId',
  justOne: false,
});

// Reverse populate with virtuals
UserSchema.virtual('paymentHistory', {
  ref: 'PaymentHistory',
  localField: '_id',
  foreignField: 'memberId',
  justOne: false,
});

// Encrypt password using bcrypt
UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next();
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

// Compare password
UserSchema.methods.comparePassword = async function (enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};

// Getting json access token and returning it
UserSchema.methods.getJwtAccessToken = function () {
  return jwt.sign(
    {
      id: this._id,
    },
    process.env.JWT_SECRET,
    { expiresIn: process.env.JWT_EXPIRES }
  );
};

// Getting json refresh token and returning it
UserSchema.methods.getJwtRefreshToken = function () {
  return jwt.sign({ id: this._id }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: process.env.REFRESH_TOKEN_SECRET_EXPIRES,
  });
};

// Cascade delete when user is deleted
UserSchema.pre('remove', async function (next) {
  await this.model('Payment').deleteMany({ memberId: this._id });
  next();
});

// Cascade delete when member is deleted
// UserSchema.pre('remove', async function (next) {
//   await this.model('PaymentHistory').deleteMany({ memberId: this._id });
//   next();
// });

// Getting the reset password token from crypto, resetting it and returning it
UserSchema.methods.getResetPasswordToken = function () {
  // const resetToken = crypto.randomBytes(20).toString('hex');

  // Hash the buffer token and set to resetPasswordToken
  // this.resetPasswordToken = crypto
  //   .createHash('sha256')
  //   .update(resetToken)
  //   .digest('hex');
  // const resetToken = Math.floor(100000 + Math.random() * 900000);
  this.resetPasswordToken = Math.floor(1000 + Math.random() * 9000);

  // Set expire
  this.resetPasswordExpiration = Date.now() + 10 * 60 * 1000;
  return this.resetPasswordToken;
};

module.exports = mongoose.model('User', UserSchema);
