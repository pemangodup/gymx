const express = require('express');
const app = express();
const dotenv = require('dotenv');
const colors = require('colors');
const morgan = require('morgan');
const connectDb = require('./config/db');
const path = require('path');
const errorHandler = require('./middleware/errors');
const compression = require('compression');
const fileupload = require('express-fileupload');

// Initializing dotenv file
dotenv.config({ path: './config/config.env' });

// JSON parser
app.use(express.json());

// For heroku process
app.use(compression());

// Static file to store phot
app.use(express.static(path.join(__dirname, 'public')));

//  File upload middleware
// app.use(fileupload());

// Initialize morgan
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Routes
const homeRoutes = require('./routes/home');
const authRoutes = require('./routes/auth');
const packageRoutes = require('./routes/package');
const userRoutes = require('./routes/users');
const paymentRoutes = require('./routes/payment');
const paymentHistoryRoutes = require('./routes/paymentHistory');
const imageRoutes = require('./routes/image');
const notificationRoutes = require('./routes/notification');
const adminRoutes = require('./routes/admin');
const memberRoutes = require('./routes/memberRoutes');

// Routing wiht the routes
app.use('/dealpha/v1/home', homeRoutes);
app.use('/dealpha/v1/auth', authRoutes);
app.use('/dealpha/v1/package', packageRoutes);
app.use('/dealpha/v1/users', userRoutes);
app.use('/dealpha/v1/payment', paymentRoutes);
app.use('/dealpha/v1/paymenthistory', paymentHistoryRoutes);
app.use('/dealpha/v1/image', imageRoutes);
app.use('/dealpha/v1/notification', notificationRoutes);
app.use('/dealpha/v1/admin', adminRoutes);
app.use('/dealpha/v1/member', memberRoutes);

// Custom error handler
app.use(errorHandler);

connectDb()
  .then(() => {
    app.listen(process.env.PORT || 3000, () => {
      console.log(`Running on port no ${process.env.PORT}`.rainbow);
    });
  })
  .catch((err) => {
    console.log(err);
  });
