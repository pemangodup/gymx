const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

const sendMail = async (options) => {
  var option = {
    auth: {
      api_key: process.env.SENDGRID_API_KEY,
    },
  };
  const transporter = nodemailer.createTransport(sgTransport(option));
  const message = {
    from: `${process.env.FROM_NAME}<${process.env.FROM_EMAIL}>`,
    to: options.email,
    subject: options.subject,
    text: options.message,
  };
  await transporter.sendMail(message);
};

module.exports = sendMail;
