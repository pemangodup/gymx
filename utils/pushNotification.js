const appID = process.env.APP_ID;
const apiKey = process.env.API_KEY;

exports.sendNotification = async function (data) {
  var headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: 'Basic ' + apiKey,
  };

  var options = {
    host: 'onesignal.com',
    port: 443,
    path: '/api/v1/notifications',
    method: 'POST',
    headers: headers,
  };

  var https = require('https');
  var req = https.request(options, function (res) {
    res.on('data', function (data) {
      console.log('Response:');
      console.log(JSON.parse(data));
    });
  });

  req.on('error', function (e) {
    console.log('ERROR:');
    console.log(e);
  });

  req.write(JSON.stringify(data));
  req.end();
};

exports.addDevice = async function (data) {
  let hehe;
  var headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };

  var options = {
    host: 'onesignal.com',
    port: 443,
    path: '/api/v1/players',
    method: 'POST',
    headers: headers,
  };

  var https = require('https');
  var req = https.request(options, function (res) {
    res.on('data', function (data) {
      console.log('Response:');
      hehe = JSON.parse(data);
      console.log(JSON.parse(data));
    });
  });

  req.on('error', function (e) {
    console.log('ERROR:');
    console.log(e);
  });

  req.write(JSON.stringify(data));
  req.end();
  return hehe;
};
