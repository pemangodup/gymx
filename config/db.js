const mongoose = require('mongoose');

const connectDb = async () => {
  if (process.env.NODE_ENV == 'production') {
    const conn = await mongoose.connect(
      `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.9wgye.mongodb.net/${process.env.MONGO_DATABASE_NAME}`
    );
  } else {
    const conn = await mongoose.connect(process.env.MONGO_URI);
  }

  // const conn = await mongoose.connect(process.env.MONGO_URI);
  // console.log(`MongoDB connected: ${conn.connection.host}`.yellow);
};

module.exports = connectDb;
