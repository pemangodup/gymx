exports.advancedResults = (model, populate) => async (req, res, next) => {
  let query;
  // Copy req.query
  const reqQuery = { ...req.query };

  // Fields to exclude
  const removeFields = ['select', 'sort', 'page', 'limit'];

  // Loop over remove and delete them from reqQuery
  removeFields.forEach((param) => delete reqQuery[param]);

  // Create query string
  let queryStr = JSON.stringify(req.query);

  // Create operators ($gt, $lt, etc.)
  queryStr = queryStr.replace(
    /\b(gt|gte|lt|lte|in)\b/g,
    (match) => `$${match}`
  );

  // Finding resources
  const loggedInUserId = JSON.stringify(req.user._id).slice(1, -1);
  const parsed = JSON.parse(queryStr);
  query = model.find({ gymId: { $eq: loggedInUserId }, parsed });

  // Select fields
  if (req.query.select) {
    const fields = req.query.select.split(',').join(' ');
    query = query.select(fields);
  }

  // Sort fields
  if (req.query.sort) {
    const sortBy = req.query.sort.split(',').join(' ');
    query = query.sort(sortBy);
  } else {
    query = query.sort('createdAt');
  }

  if (req.query.restriction) {
    query = query.find({ role: { $ne: `${req.query.restriction}` } });
  }

  // Pagination
  const page = parseInt(req.query.page, 10) || 1;
  const limit = parseInt(req.query.limit, 10) || 10;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  // countDocuments() mongoose methods
  const total = await model.countDocuments();

  query = query.skip(startIndex).limit(limit);

  // Adding populate in query for populating it
  if (populate) {
    populate.forEach((element) => {
      query = query.populate(element);
    });
  }

  // Executing query
  const results = await query;

  // Pagination result
  const pagination = {};

  // Show or dont show previous page
  if (endIndex < total) {
    pagination.next = {
      page: page + 1,
      limit,
    };
  }

  if (startIndex > 0) {
    pagination.prev = {
      page: page - 1,
      limit,
    };
  }
  // For sorting in paid date. Latest data on the top
  results.sort(
    (a, b) =>
      new Date(b.createdAt || b.paidDate) - new Date(a.createdAt || a.paidDate)
  );

  res.advancedResults = {
    success: true,
    message: '',
    count: results.length,
    // pagination,
    data: results,
  };

  next();
};
