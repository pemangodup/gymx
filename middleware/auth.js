const jwt = require('jsonwebtoken');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const User = require('../models/User');
const Admin = require('../models/Admin');

exports.protect = asyncHandler(async (req, res, next) => {
  let token;

  // Checking whether there is token or not
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  }
  // Make sure the token exists
  if (!token) {
    return next(new ErrorResponse('Not allowed to access', 403));
  }

  try {
    // verify token
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = await Admin.findById(decoded.id);
    next();
  } catch (error) {
    return next(new ErrorResponse('Token expired', 401));
  }
});

// Grand access to specific user
exports.authorize = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(new ErrorResponse('User is not authorized', 403));
    }
    next();
  };
};
